from django.shortcuts import redirect, render
from django.views.generic.detail import DetailView


from apps.perfil.models import Perfil
from datetime import date, timedelta
from apps.portafolio.models import Portafolio, PortafolioImage

from apps.skill.models import Skill
from apps.resumen.models import Educacion,Experiencia

def home( request ):
    dataperfil = Perfil.objects.get( pk=1 )

    meses=['Enero','Febrero','marzo','Abril','Mayo','Junio','julio','Agosto','Septiembre','Octubreo','Nobiembre','Diciembre']
    age = (date.today() - dataperfil.cumple) // timedelta(days=365.2425)
    perfil={
        'perfil':dataperfil.perfil,
        'nombres':dataperfil.first_name, 
        'apellidos': dataperfil.last_name,
        'corroe': dataperfil.email,
        'gitlab': dataperfil.gitlab,
        'github': dataperfil.github,
        'linkedin':dataperfil.linkedin,
        'cumple':"{} {}, del {}".format( meses[ dataperfil.cumple.month - 1 ], +dataperfil.cumple.day, dataperfil.cumple.year ),
        'edad':age,
        'titulo':dataperfil.titulo,
        'detalle':dataperfil.detalle,
        'ciudad':dataperfil.ciudad,
        'detalleactual':dataperfil.detalleactual,
        'imagen':dataperfil.imagen,
        'resumen':dataperfil.resumen,
    }
    skils = Skill.objects.all()

    edu = Educacion.objects.all().order_by('-id')
    exp = Experiencia.objects.all().order_by('-id')

    porductos = Portafolio.objects.all()

    por=[]
    for produc in porductos:
        imagen = PortafolioImage.objects.filter(post=produc).first()
        por.append({
            'titulo':produc.titulo,
            'slug':produc.slug,
            'categoria':produc.categoria,
            'image':imagen
        })


    return render( 
        request,
        'home/home.html',
        {
            'perfil':perfil,
            'skills':skils,
            'educacion':edu,
            'experiencia':exp,
            'prtafolios':por
        }
    )

class Portafofilo(DetailView):
    """Detail post."""
    template_name = 'portafolios/portfolio-details.html'
    model= Portafolio

    def get_context_data( self, **kwargs ):
        contex =  super().get_context_data( **kwargs )
       
        dataperfil = Perfil.objects.get( pk=1 )
        perfil={
            'perfil':dataperfil.perfil,
            'nombres':dataperfil.first_name, 
            'apellidos': dataperfil.last_name,
            'corroe': dataperfil.email,
            'gitlab': dataperfil.gitlab,
            'github': dataperfil.github,
            'linkedin':dataperfil.linkedin,
        }
        contex['perfil']=perfil
        imagenes = PortafolioImage.objects.filter(post=contex['portafolio'])
        contex['imagenes']=imagenes


        return contex