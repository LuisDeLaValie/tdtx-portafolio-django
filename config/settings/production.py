from .base import *

import dj_database_url
from decouple import config


# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = [
    "tdtx-portafolio-django.herokuapp.com"
]


# Database
# https://docs.djangoproject.com/en/3.2/ref/settings/#databases

DATABASES = {
    "default": dj_database_url.config(
        default=config('DATABASE_URL')
    )
}
# DATABASES = {
#     "default": {
#         "ENGINE": "django.db.backends.postgresql_psycopg2",
#         "NAME": "[YOUR_DATABASE_NAME]",
#         "USER": "[YOUR_USER_NAME]",
#         "PASSWORD": "",
#         "HOST": "localhost",
#         "PORT": "",
#     }
# }
