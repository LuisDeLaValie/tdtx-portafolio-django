from django.apps import AppConfig


class MymarkdownConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'apps.myMarkdown'
