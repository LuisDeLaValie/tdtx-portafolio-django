# Generated by Django 3.2.8 on 2021-10-11 03:28

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Educacion',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('titulo', models.CharField(blank=True, max_length=50, null=True)),
                ('inicio', models.CharField(max_length=10)),
                ('final', models.CharField(max_length=10)),
                ('instituto', models.CharField(max_length=100)),
                ('localidad', models.CharField(max_length=100)),
                ('contenido', models.TextField(blank=True, null=True)),
                ('create_add', models.DateTimeField(auto_now_add=True)),
            ],
        ),
        migrations.CreateModel(
            name='Experiencia',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('puesto', models.CharField(blank=True, max_length=50, null=True)),
                ('inicio', models.CharField(max_length=10)),
                ('final', models.CharField(max_length=10)),
                ('instituto', models.CharField(max_length=100)),
                ('localidad', models.CharField(max_length=100)),
                ('contenido', models.TextField(blank=True, null=True)),
                ('create_add', models.DateTimeField(auto_now_add=True)),
            ],
        ),
    ]
