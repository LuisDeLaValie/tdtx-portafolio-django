from django.contrib import admin

from .models import Educacion, Experiencia



class EducacionAdmin(admin.ModelAdmin):
    fields = ('titulo','inicio','final','instituto','localidad','contenido')
    list_display = ('instituto','inicio','final')
    
admin.site.register( Educacion, EducacionAdmin )


class ExperienciaAdmin(admin.ModelAdmin):
    fields = ('puesto','inicio','final','instituto','localidad','contenido')
    list_display = ('instituto','inicio','final')

admin.site.register( Experiencia, ExperienciaAdmin )

