from django.db import models

# Create your models here.
class Educacion( models.Model ):
    titulo = models.CharField( max_length=50, null=True, blank=True)
    inicio = models.CharField( max_length=10, null=False, blank=False)
    final = models.CharField( max_length=10, null=False, blank=False)
    instituto = models.CharField( max_length=100, null=False, blank=False)
    localidad = models.CharField( max_length=100, null=False, blank=False)
    contenido = models.TextField(null=True, blank=True)
    create_add = models.DateTimeField(auto_now_add=True)

class Experiencia( models.Model ):
    puesto = models.CharField( max_length=50, null=True, blank=True)
    inicio = models.CharField( max_length=10, null=False, blank=False)
    final = models.CharField( max_length=10, null=False, blank=False)
    instituto = models.CharField( max_length=100, null=False, blank=False)
    localidad = models.CharField( max_length=100, null=False, blank=False)
    contenido = models.TextField(null=True, blank=True)
    create_add = models.DateTimeField(auto_now_add=True)