from django.contrib import admin

from .models import Skill

class SkillAdmin(admin.ModelAdmin):
    fields = ('titulo', 'classaicon')
    list_display = ('titulo','classaicon')


# Register your models here.
admin.site.register( Skill, SkillAdmin )
