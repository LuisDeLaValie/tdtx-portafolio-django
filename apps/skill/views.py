from django.shortcuts import render
from django.views.generic.list import ListView


from .models import Skill
# Create your views here.


class SkillListView(ListView):
    template_name = 'home/skills.html'
    queryset = Skill.objects.all().order_by('-id')

    def get_context_data( self, **kwargs ):
        contex =  super().get_context_data( **kwargs )
        contex['message'] = 'Listado de Productos'
        print(contex)

        return contex
