from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import Perfil

class PerlUser(UserAdmin):
    fieldsets = (
        (None, {'fields': ('username', 'password')}),
        (('Personal info'), {'fields': (
            'perfil',
            'first_name',
            'last_name',
            'email',
            'gitlab',
            'github',
            'linkedin',
            'cumple',
            'titulo',
            'ciudad',
            'detalle',
            'detalleactual',
            'imagen', 
            'resumen'
            )
        }),
        (('Permissions'), {
            'fields': ('is_active', 'is_staff', 'is_superuser', 'groups', 'user_permissions'),
        }),
        (('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )

    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': (
                'perfil',
                'username',
                'password1',
                'password2',
                'first_name',
                'last_name',
                'email',
                'gitlab',
                'github',
                'linkedin',
                'cumple',
                'titulo',
                'ciudad',
                'detalle',
                'detalleactual',
                'imagen'
            ),
        }),
    )


admin.site.register( Perfil, PerlUser )
