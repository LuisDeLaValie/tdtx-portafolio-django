from django.db import models


from django.contrib.auth.models import AbstractUser

from gdstorage.storage import GoogleDriveStorage

gd_storage = GoogleDriveStorage()

# Create your models here.
class Perfil(AbstractUser):
    perfil = models.ImageField( upload_to='Perfil/') 
    gitlab = models.CharField( max_length=50, null=True, blank=True)
    github = models.CharField( max_length=50, null=True, blank=True)
    linkedin = models.CharField( max_length=50, null=True, blank=True)

    cumple = models.DateField(null=False, blank=False, default='2000-02-21')
    titulo = models.CharField( max_length=50, )
    ciudad = models.CharField( max_length=50, )
    detalle = models.TextField(null=True, blank=True)
    detalleactual = models.TextField(null=True, blank=True)
    imagen = models.ImageField( upload_to='Perfil/')
    
    resumen = models.TextField(null=True, blank=True)
    


    def get_full_name( self ):
        return '{} {}'.format( self.first_name, self.last_name )
    
    def save(self, *args, **kwargs):
        if self.pk:
            try:
                old_perfil = Perfil.objects.get(pk=self.pk)
            except Perfil.DoesNotExist:
                return
            else:
                if old_perfil and old_perfil.perfil.url != self.perfil.url:
                    old_perfil.perfil.delete(save=False)
                
                if old_perfil and old_perfil.imagen.url != self.imagen.url:
                    old_perfil.perfil.delete(save=False)
                    
        super(Perfil, self).save(*args, **kwargs)

