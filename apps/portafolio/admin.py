from django.contrib import admin

from .models import Portafolio, PortafolioImage
class PortafolioImageAdmin(admin.StackedInline):
    model = PortafolioImage
class PortafolioAdmin(admin.ModelAdmin):
    fields = ('titulo','categoria','cliente','Fecha','proyecto','description')
    inlines=[PortafolioImageAdmin]
    list_display = ('__str__', 'slug','create_at')


# Register your models here.
admin.site.register( Portafolio, PortafolioAdmin )