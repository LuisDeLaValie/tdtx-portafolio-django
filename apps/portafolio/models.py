from django.db import models

from django.utils.text import slugify

import uuid

from gdstorage.storage import GoogleDriveStorage

gd_storage = GoogleDriveStorage()
class Portafolio(models.Model):
    titulo = models.CharField( max_length=50 )
    slug =  models.SlugField( null=False, blank=False, unique=True )
    categoria = models.CharField( max_length=100, null=False, blank=False)
    cliente = models.CharField( max_length=50, null=True, blank=True)
    proyecto = models.CharField( max_length=100, null=True, blank=True)
    Fecha = models.DateField(null=False, blank=False )
    description = models.TextField()
    create_at = models.DateTimeField( auto_now_add=True )

    

    def __str__(self) -> str:
        return self.titulo

    def save( self, *args, **kwargs ):

        slug = slugify( self.titulo )        
        exist =Portafolio.objects.filter(slug=slug).exists()

        if exist:
            self.slug = slugify( '{}-{}'.format( self.titulo, str( uuid.uuid4() )[:8] ) )
        else:
            self.slug = slug

        super( Portafolio,self ).save( *args, **kwargs )

class PortafolioImage(models.Model):
    post = models.ForeignKey(Portafolio, default=None, on_delete=models.CASCADE)
    images = models.ImageField(upload_to = 'products/' )